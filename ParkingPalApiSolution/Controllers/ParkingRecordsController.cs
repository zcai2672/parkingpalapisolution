﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ParkingPalApiSolution;

namespace ParkingPalApiSolution.Controllers
{
    public class ParkingRecordsController : ApiController
    {
        private ParkingPalSQLEntities db = new ParkingPalSQLEntities();

        // GET: api/ParkingRecords
        public IQueryable<ParkingRecord> GetParkingRecords()
        {
            return db.ParkingRecords;
        }

        // GET: api/ParkingRecords/5
        [ResponseType(typeof(ParkingRecord))]
        public IHttpActionResult GetParkingRecord(int id)
        {
            ParkingRecord parkingRecord = db.ParkingRecords.Find(id);
            if (parkingRecord == null)
            {
                return NotFound();
            }

            return Ok(parkingRecord);
        }

        // PUT: api/ParkingRecords/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutParkingRecord(int id, ParkingRecord parkingRecord)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != parkingRecord.RecordId)
            {
                return BadRequest();
            }

            db.Entry(parkingRecord).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ParkingRecordExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/ParkingRecords
        [ResponseType(typeof(ParkingRecord))]
        public IHttpActionResult PostParkingRecord(ParkingRecord parkingRecord)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.ParkingRecords.Add(parkingRecord);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = parkingRecord.RecordId }, parkingRecord);
        }

        // DELETE: api/ParkingRecords/5
        [ResponseType(typeof(ParkingRecord))]
        public IHttpActionResult DeleteParkingRecord(int id)
        {
            ParkingRecord parkingRecord = db.ParkingRecords.Find(id);
            if (parkingRecord == null)
            {
                return NotFound();
            }

            db.ParkingRecords.Remove(parkingRecord);
            db.SaveChanges();

            return Ok(parkingRecord);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ParkingRecordExists(int id)
        {
            return db.ParkingRecords.Count(e => e.RecordId == id) > 0;
        }
    }
}