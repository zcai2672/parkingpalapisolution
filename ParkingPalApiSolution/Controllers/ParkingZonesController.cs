﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ParkingPalApiSolution;
using WebGrease.Css.Ast.Selectors;

namespace ParkingPalApiSolution.Controllers
{
    [RoutePrefix("api/parkingZones")]
    public class ParkingZonesController : ApiController
    {
        private ParkingPalSQLEntities db = new ParkingPalSQLEntities();

        // GET: api/ParkingZones
//        public IQueryable<ParkingZone> GetParkingZones()
//        {
//            return db.ParkingZones;
//
//        }

     //   [Route("LocalParkingZones")]
        public List<ParkingZone> GetParkingZones( double? lat, double? lon, double? radius = 0.001 )
        {
         //   double radius = 0.001;

            List<ParkingZone> localZones = (from z in db.ParkingZones
                                            where 
                                                z.Lat > lat- radius && z.Lat < lat + radius 
                                                && z.Long > lon - radius && z.Long < lon + radius
                                            select z )
                                            .ToList();

            return localZones;
        }


        // GET: api/ParkingZones/5
        [ResponseType(typeof(ParkingZone))]
        public IHttpActionResult GetParkingZone(int id)
        {
            ParkingZone parkingZone = db.ParkingZones.Find(id);
            if (parkingZone == null)
            {
                return NotFound();
            }

            return Ok(parkingZone);
        }

        // PUT: api/ParkingZones/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutParkingZone(int id, ParkingZone parkingZone)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != parkingZone.ZoneId)
            {
                return BadRequest();
            }

            db.Entry(parkingZone).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ParkingZoneExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/ParkingZones
        [ResponseType(typeof(ParkingZone))]
        public IHttpActionResult PostParkingZone(ParkingZone parkingZone)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.ParkingZones.Add(parkingZone);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (ParkingZoneExists(parkingZone.ZoneId))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = parkingZone.ZoneId }, parkingZone);
        }

        // DELETE: api/ParkingZones/5
        [ResponseType(typeof(ParkingZone))]
        public IHttpActionResult DeleteParkingZone(int id)
        {
            ParkingZone parkingZone = db.ParkingZones.Find(id);
            if (parkingZone == null)
            {
                return NotFound();
            }

            db.ParkingZones.Remove(parkingZone);
            db.SaveChanges();

            return Ok(parkingZone);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ParkingZoneExists(int id)
        {
            return db.ParkingZones.Count(e => e.ZoneId == id) > 0;
        }
    }
}