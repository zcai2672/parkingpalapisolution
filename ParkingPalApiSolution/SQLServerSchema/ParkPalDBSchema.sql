USE [master]
GO
/****** Object:  Database [ParkingPalSQL]    Script Date: 30/07/2017 3:49:17 PM ******/
CREATE DATABASE [ParkingPalSQL]
GO
ALTER DATABASE [ParkingPalSQL] SET COMPATIBILITY_LEVEL = 130
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [ParkingPalSQL].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [ParkingPalSQL] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [ParkingPalSQL] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [ParkingPalSQL] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [ParkingPalSQL] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [ParkingPalSQL] SET ARITHABORT OFF 
GO
ALTER DATABASE [ParkingPalSQL] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [ParkingPalSQL] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [ParkingPalSQL] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [ParkingPalSQL] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [ParkingPalSQL] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [ParkingPalSQL] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [ParkingPalSQL] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [ParkingPalSQL] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [ParkingPalSQL] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [ParkingPalSQL] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [ParkingPalSQL] SET ALLOW_SNAPSHOT_ISOLATION ON 
GO
ALTER DATABASE [ParkingPalSQL] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [ParkingPalSQL] SET READ_COMMITTED_SNAPSHOT ON 
GO
ALTER DATABASE [ParkingPalSQL] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [ParkingPalSQL] SET  MULTI_USER 
GO
ALTER DATABASE [ParkingPalSQL] SET DB_CHAINING OFF 
GO
ALTER DATABASE [ParkingPalSQL] SET ENCRYPTION ON
GO
ALTER DATABASE [ParkingPalSQL] SET QUERY_STORE = ON
GO
ALTER DATABASE [ParkingPalSQL] SET QUERY_STORE (OPERATION_MODE = READ_WRITE, CLEANUP_POLICY = (STALE_QUERY_THRESHOLD_DAYS = 30), DATA_FLUSH_INTERVAL_SECONDS = 900, INTERVAL_LENGTH_MINUTES = 60, MAX_STORAGE_SIZE_MB = 100, QUERY_CAPTURE_MODE = AUTO, SIZE_BASED_CLEANUP_MODE = AUTO)
GO
USE [ParkingPalSQL]
GO
ALTER DATABASE SCOPED CONFIGURATION SET IDENTITY_CACHE = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
USE [ParkingPalSQL]
GO
/****** Object:  Table [dbo].[ParkingRecords]    Script Date: 30/07/2017 3:49:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ParkingRecords](
	[RecordId] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[VehicleId] [nchar](10) NOT NULL,
	[ZoneId] [int] NOT NULL,
	[StartTime] [timestamp] NOT NULL,
	[EndTime] [time](7) NULL,
	[ParkingStatus] [nvarchar](50) NULL,
 CONSTRAINT [PK_ParkingRecords] PRIMARY KEY CLUSTERED 
(
	[RecordId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
GO
/****** Object:  Table [dbo].[ParkingZone]    Script Date: 30/07/2017 3:49:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ParkingZone](
	[ZoneId] [int] NOT NULL,
	[Category] [nvarchar](100) NULL,
	[Street] [nvarchar](50) NULL,
	[Suburb] [nvarchar](50) NULL,
	[StayHours] [int] NULL,
	[Restrictions] [nvarchar](150) NULL,
	[OperationalDays] [nvarchar](50) NULL,
	[OperationTime] [nvarchar](100) NULL,
	[Rate] [float] NULL,
	[LocationDesc] [nvarchar](200) NULL,
	[Bays] [int] NULL,
	[Long] [float] NULL,
	[Lat] [float] NULL,
 CONSTRAINT [PK_ParkingZone] PRIMARY KEY CLUSTERED 
(
	[ZoneId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
GO
/****** Object:  Table [dbo].[TestTable]    Script Date: 30/07/2017 3:49:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TestTable](
	[id] [int] NOT NULL,
	[lat] [float] NULL,
	[long] [float] NULL,
	[rate] [float] NULL,
 CONSTRAINT [PK_TestTable] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
GO
/****** Object:  Table [dbo].[User]    Script Date: 30/07/2017 3:49:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[UserId] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nchar](10) NOT NULL,
	[SurName] [nchar](10) NOT NULL,
	[Address] [nvarchar](50) NULL,
	[LicenceNumber] [nchar](10) NOT NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
GO
/****** Object:  Table [dbo].[UserVehicle]    Script Date: 30/07/2017 3:49:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserVehicle](
	[UserId] [int] NOT NULL,
	[VehicleId] [nchar](10) NOT NULL,
 CONSTRAINT [PK_UserVehicle] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[VehicleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
GO
/****** Object:  Table [dbo].[Vehicle]    Script Date: 30/07/2017 3:49:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Vehicle](
	[VehicleId] [nchar](10) NOT NULL,
	[Make] [nvarchar](50) NOT NULL,
	[Year] [int] NULL,
	[Model] [nvarchar](50) NULL,
 CONSTRAINT [PK_Vehicle] PRIMARY KEY CLUSTERED 
(
	[VehicleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
GO
ALTER TABLE [dbo].[ParkingRecords]  WITH CHECK ADD  CONSTRAINT [FK_ParkingRecords_ParkingZone] FOREIGN KEY([ZoneId])
REFERENCES [dbo].[ParkingZone] ([ZoneId])
GO
ALTER TABLE [dbo].[ParkingRecords] CHECK CONSTRAINT [FK_ParkingRecords_ParkingZone]
GO
ALTER TABLE [dbo].[ParkingRecords]  WITH CHECK ADD  CONSTRAINT [FK_ParkingRecords_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([UserId])
GO
ALTER TABLE [dbo].[ParkingRecords] CHECK CONSTRAINT [FK_ParkingRecords_User]
GO
ALTER TABLE [dbo].[ParkingRecords]  WITH CHECK ADD  CONSTRAINT [FK_ParkingRecords_Vehicle] FOREIGN KEY([VehicleId])
REFERENCES [dbo].[Vehicle] ([VehicleId])
GO
ALTER TABLE [dbo].[ParkingRecords] CHECK CONSTRAINT [FK_ParkingRecords_Vehicle]
GO
ALTER TABLE [dbo].[UserVehicle]  WITH CHECK ADD  CONSTRAINT [FK_UserVehicle_User1] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([UserId])
GO
ALTER TABLE [dbo].[UserVehicle] CHECK CONSTRAINT [FK_UserVehicle_User1]
GO
ALTER TABLE [dbo].[UserVehicle]  WITH CHECK ADD  CONSTRAINT [FK_UserVehicle_Vehicle1] FOREIGN KEY([VehicleId])
REFERENCES [dbo].[Vehicle] ([VehicleId])
GO
ALTER TABLE [dbo].[UserVehicle] CHECK CONSTRAINT [FK_UserVehicle_Vehicle1]
GO
USE [master]
GO
ALTER DATABASE [ParkingPalSQL] SET  READ_WRITE 
GO
